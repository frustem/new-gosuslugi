import React from 'react'
import Form from 'react-jsonschema-form'
import './Form.scss'
import ArrayFieldTemplate from "./ArrayFieldTemplate"
import RadioWidget from "./RadioWidget"
import ErrorList from './ErrorList'

const customWidgets = { RadioWidget: RadioWidget }

const ReactForm = (props) =>
  <Form ArrayFieldTemplate={ArrayFieldTemplate} widgets={customWidgets} ErrorList={ErrorList} {...props} />

export default ReactForm

