import React from 'react'
import {storiesOf} from '@storybook/react'
import {withInfo} from '@storybook/addon-info'
import {withKnobs} from '@storybook/addon-knobs/react'
import Form from './Form'
import SelectWidget from './SelectWIdget'

const arraySchema = {
  definitions: {
    Thing: {
      type: 'object',
      properties: {
        name: {
          type: 'string',
          default: 'Default name',
          minLength: 3
        },
        surname: {
          type: 'string',
          placeholder: 'surname'
        }
      }
    }
  },
  type: 'object',
  properties: {
    fixedItemsList: {
      type: 'array',
      title: 'A list of fixed items',
      items: [
        {
          title: 'A string value',
          type: 'string',
          default: 'lorem ipsum'
        },
        {
          title: 'A boolean value',
          type: 'boolean',
          default: false
        }
      ],
      additionalItems: {
        title: 'Addtional item',
        description: 'descr',
        type: 'string'
      }
    }
  
  }
}
const arrayUISchema = {
  classNames: 'ararar',
  listOfStrings: {
    items: {
      'ui:emptyValue': 'dd'
    }
  },
  multipleChoicesList: {
    'ui:widget': 'checkboxes'
  },
  fixedItemsList: {
    items: [
      {
        'ui:widget': 'textarea',
      },
      {
        'ui:widget': SelectWidget
      }
    ]
  },
  unorderable: {
    'ui:options': {
        orderable: false
    }
  },
  unremovable: {
    'ui:options': {
      removable: false
    }
  },
  noToolbar: {
    'ui:options': {
      orderable: false,
      removable: false,
      addable: false
    }
  },
  fixedNoToolbar: {
    'ui:options': {
      orderable: false,
      removable: false,
      addable: false
    }
  }
}
const arrayFormData = {
  "listOfStrings": [
    "sdffoo",
    "bar"
  ],
  "multipleChoicesList": [
    "foo",
    "bar"
  ],
  "fixedItemsList": [
    "Some text",
    true,
    123
  ]

}

const selectSchema = {
  "definitions": {
  "largeEnum": {
    "type": "string",
      "enum": [
      "option #0",
      "option #1",
      "option #2",
      "option #3",
      "option #4",
      "option #5",
      "option #6",
      "option #7",
      "option #8",
      "option #9",
      "option #10",
      "option #11",
      "option #12",
      "option #13",
      "option #14",
      "option #15",
      "option #16",
      "option #17",
      "option #18",
      "option #19",
      "option #20",
      "option #21",
      "option #22",
      "option #23",
    ]
  }
},
  "title": "A rather large form",
  "type": "object",
  "properties": {
    "choice1": {
      "$ref": "#/definitions/largeEnum"
    },
    "choice2": {
      "$ref": "#/definitions/largeEnum"
    },
    "choice3": {
      "$ref": "#/definitions/largeEnum"
    }
  }
}
const selectUISchema = {
  'choice1': {
    'ui:widget': SelectWidget
  },
  'choice2': {
    'ui:widget': SelectWidget
  },
  'choice3': {
    'ui:widget': SelectWidget
  }
}



storiesOf('Элементы формы', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('ArrayForm', () => (
    <div className="container">
      <Form
        schema={arraySchema}
        uiSchema={arrayUISchema}
        formData={arrayFormData}
        liveValidate
      />
    </div>
  ))
  .add('SelectSchema', () => (
    <div className="container">
      <Form
        schema={selectSchema}
        uiSchema={selectUISchema}
        liveValidate
      />
    </div>
  ))

