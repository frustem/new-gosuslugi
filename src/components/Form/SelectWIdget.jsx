import React from "react";
import PropTypes from "prop-types";
import Select from 'react-select'

import { asNumber, guessType } from "react-jsonschema-form/lib/utils";

const nums = new Set(["number", "integer"]);

/**
 * This is a silly limitation in the DOM where option change event values are
 * always retrieved as strings.
 */
function processValue(schema, value) {
  console.log(value)
  // "enum" is a reserved word, so only "type" and "items" can be destructured
  const { type, items } = schema
  if (value === "") {
    return undefined
  } else if (type === "array" && items && nums.has(items.type)) {
    return value.map(asNumber)
  } else if (type === "boolean") {
    return value === "true"
  } else if (type === "number") {
    return asNumber(value)
  }
  
  // If type is undefined, but an enum is present, try and infer the type from
  // the enum values
  if (schema.enum) {
    if (schema.enum.every(x => guessType(x) === "number")) {
      return asNumber(value)
    } else if (schema.enum.every(x => guessType(x) === "boolean")) {
      return value === "true"
    }
  }
  
  return value
}

function SelectWidget(props){
  const {
    schema,
    id,
    options,
    value,
    required,
    disabled,
    readonly,
    multiple,
    autofocus,
    onChange,
    onBlur,
    onFocus,
    placeholder
  } = props
  const { enumOptions, enumDisabled } = options
  const emptyValue = multiple ? [] : ""
  const customStyles = {
    container: (provided) => ({
      ...provided,
      marginRight: '20px'
    }),
    control: (provided, { isFocused }) => ({
      ...provided,
      borderRadius: '4px',
      borderColor: isFocused ? '#6CA9D2 ' : 'gray',
      boxShadow: isFocused ? '0 0 0 3px #6CA9D2' : 'none',
      '&:hover': {
        borderColor: isFocused ? '#6CA9D2' : 'gray',
      },
    }),
    input: () => ({
      width: '200px',
      padding: '20px'
    }),
    option: (provided, state) => ({
      ...provided,
      display: 'flex',
      alignItems: 'center',
      height: '54px',
      paddingLeft: '20px',
      color: 'black',
      backgroundColor: state.isFocused ? '#6CA9D2' : 'white',
      '&:before': {
        content: '""',
        display: state.isSelected ? 'inline-block' : 'none',
        marginRight: '10px',
        borderBottom: '1px solid black',
        borderRight: '1px solid black',
        width: '10px',
        height: '10px',
        transform: 'rotate(45deg)'
      }
    })
  }
  
  return (
    <Select
      id={id}
      multiple={multiple}
      className="react-select"
      styles={customStyles}
      value={typeof value === "undefined" ? emptyValue : {value: value, label: value}}
      required={required}
      placeholder={placeholder}
      disabled={disabled || readonly}
      autoFocus={autofocus}
      onBlur={onBlur}
      onFocus={onFocus}
      onChange={selectedOption => {
        const newValue = selectedOption.value;
        onChange(processValue(schema, newValue));
      }}
      options={enumOptions}
    />
  )
  
}

SelectWidget.defaultProps = {
  autofocus: false
}

if (process.env.NODE_ENV !== "production") {
  SelectWidget.propTypes = {
    schema: PropTypes.object.isRequired,
    id: PropTypes.string.isRequired,
    options: PropTypes.shape({
      enumOptions: PropTypes.array,
    }).isRequired,
    value: PropTypes.any,
    required: PropTypes.bool,
    disabled: PropTypes.bool,
    readonly: PropTypes.bool,
    multiple: PropTypes.bool,
    autofocus: PropTypes.bool,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    onFocus: PropTypes.func,
  };
}

export default SelectWidget;
