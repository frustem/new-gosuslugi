import React from 'react'
import {storiesOf} from '@storybook/react'
import {withInfo} from '@storybook/addon-info'
import {withKnobs} from '@storybook/addon-knobs/react'
import Form from './Form'

const arraySchema = {
  definitions: {
    Thing: {
      type: 'object',
      properties: {
        name: {
          type: 'string',
          default: 'Default name',
          minLength: 3
        },
        surname: {
          type: 'string',
          placeholder: 'surname'
        },
        number: {
          type: 'number',
          label: 'NUMBER'
        }
      }
    }
  },
  type: 'object',
  properties: {
     minItemsList: {
      type: 'array',
      title: 'A list with minimal number of items',
      minItems: 3,
      items: {
        $ref: '#/definitions/Thing'
      }
    },
 
  }
}

const arrayUISchema = {
  listOfStrings: {
    items: {
      'ui:emptyValue': 'dd'
    }
  },
  unorderable: {
    'ui:options': {
      orderable: false
    }
  },
  unremovable: {
    'ui:options': {
      removable: false
    }
  },
  noToolbar: {
    'ui:options': {
      orderable: false,
      removable: false,
      addable: false
    }
  },
  fixedNoToolbar: {
    'ui:options': {
      orderable: false,
      removable: false,
      addable: false
    }
  }
}

const radioSchema = {
  type: 'string',
  title: 'RADIO',
  oneOf: [
    {
      title: 'Plus',
      const: 'PLUS'
    },
    {
      title: 'Minus',
      const: 'MINUS'
    },
    {
      title: 'Not change',
      const: 'NOT CHANGE'
    }
  ]
}

const checkSchema = {
  title: 'CHECK',
  type: 'array',
  items: {
    type: 'string',
    enum: [
      'foo',
      'bar',
      'fuz',
      'quxx'
    ]
  },
  uniqueItems: true
}

const radioUISchema = {
  'ui:widget': 'radio'
}
const checkUISchema = {
  'ui:widget': 'checkboxes'
}

storiesOf('Элементы формы', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('ArrayForum', () => (
    <div className="container">
      <Form
        schema={arraySchema}
        uiSchema={arrayUISchema}
        className="arrayClass"
        formData={{}}
        liveValidate
      />
    </div>
  ))
  .add('Radio', () => (
    <div className="container">
      <Form
        schema={radioSchema}
        uiSchema={radioUISchema}
      />
    </div>
  ))
  .add('Check', () => (
    <div className="container">
      <Form
        schema={checkSchema}
        uiSchema={checkUISchema}
      />
    </div>
  ))

