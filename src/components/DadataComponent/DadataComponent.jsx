import React from 'react'
import { ReactDadata } from 'react-dadata'

const TOKEN = '47a3d10e2f0f10096c515bc50734b158e18396e7'

export default (props) => (
  <ReactDadata
    token={TOKEN}
    type="NAME"
    autocomplete="true"
    query=""
    placholder=""
    {...props}
  />
)