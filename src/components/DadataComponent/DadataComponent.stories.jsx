import React from 'react'
import {storiesOf} from '@storybook/react'
import {withInfo} from '@storybook/addon-info'
import {withKnobs, text} from '@storybook/addon-knobs/react'
import DadataComponent from './DadataComponent'

storiesOf('DADATA', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .add('DadataComponent', () => (
    <DadataComponent>
      {text('Button text', 'Button text')}
    </DadataComponent>
  ))