import React from 'react'
import {storiesOf} from '@storybook/react'
// import {withInfo} from '@storybook/addon-info'
import {withKnobs} from '@storybook/addon-knobs/react'
import Breadcrumb from './Breadcrumb'
// import './Breadcrumb.sass'


storiesOf('Основные составляющие страницы/Breadcrumb', module)
  .addDecorator(withKnobs)
  // .addDecorator(withInfo)
  .add('Breadcrumb', () => (
    <Breadcrumb>
      <Breadcrumb.Item href="#">Home</Breadcrumb.Item>
      <Breadcrumb.Item href="#">
        Library
      </Breadcrumb.Item>
      <Breadcrumb.Item active>Data</Breadcrumb.Item>
    </Breadcrumb>
  ))