import React from 'react'
import {storiesOf} from '@storybook/react'
import {withInfo} from '@storybook/addon-info'
import {withKnobs, boolean, select} from '@storybook/addon-knobs/react'
import Loader from './index'

const Size = 'size'
const optionsSize = {
  lg: 'lg',
  '': 'md',
  sm: 'sm',
  xs: 'xs'
}
const defaultValueSize = ''

const isLoading = 'isLoading'
const defaultValueIsLoading = true

storiesOf('Графические элементы/Прелоадер', module)
  .addDecorator(withKnobs)
  .add('Loader', withInfo('')(() => (
    <Loader
      key="2"
      isLoading={boolean(isLoading, defaultValueIsLoading)}
      size={select(Size, optionsSize, defaultValueSize)}
    />
  )))

