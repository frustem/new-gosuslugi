---
### Компонент `<Loader />`

Используйте KNOBS (панель снизу) для изменения значений Property

| Property  | PropType | Values                   | DefaultProps |
| --------- | -------- | ------------------------ | ------------ |
| isLoading | boolean  |                          | false        |
| bsSize    | string   | one of: "lg", "sm", "xs" | ''           |

---
