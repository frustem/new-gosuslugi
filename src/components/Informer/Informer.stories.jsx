import React from 'react'
import {storiesOf} from '@storybook/react'
import {withInfo} from '@storybook/addon-info'
import {withKnobs, text, select} from '@storybook/addon-knobs/react'
import Informer from './index'

const typeOptions = {
  hex_approve_green: 'hex_approve_green',
  hex_switch: 'hex_switch',
  hex_warn_red: 'hex_warn_red',
  mail_icon: 'mail_icon',
  settings_gray: 'settings_gray',
  settings_white: 'settings_white'
}

const variantOptions = {
  default: 'default',
  danger: 'danger'
}

const DEFAULT_TEXT = 'Текст события - Plain text.'

storiesOf('Основные составляющие страницы/Информеры', module)
  .addDecorator(withKnobs)
  .add('Informer', withInfo('')(() => (
    <Informer
      type={select('type', typeOptions, 'hex_approve_green')}
      title={text('title', 'Title')}
      variant={select('variant', variantOptions, 'default')}
    >
      {text('text', DEFAULT_TEXT)}
    </Informer>
  )))