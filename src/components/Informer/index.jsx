import React from 'react'
import PropTypes from 'prop-types'
import Icon from '../Icon'
import './Informer.sass'

const Informer = ({  type, title, children, variant }) => (
  <div className="informer">
    <div className="informer__caption-wrapper">
      <div className="icon-informer">
        <Icon type={type} />
      </div>
      <span className="informer__caption">{title}</span>
    </div>
    
    <div className={`informer__text ${variant}`}>
      {children}
    </div>
  </div>
)

Informer.defaultProps = {
  variant: 'default'
}

Informer.propTypes = {
  type:  PropTypes.isRequired,
  title: PropTypes.string.isRequired,
  children: PropTypes.string.isRequired,
  variant: PropTypes.oneOf(['default', 'danger'])
}

export default Informer