import React from 'react'
import {storiesOf} from '@storybook/react'
import {withInfo} from '@storybook/addon-info'
import {withKnobs} from '@storybook/addon-knobs/react'
import NotificationList from './index'

const DEFAULT_NOTIFICATIONS = [
  { id: 1, type: 'close', title: 'Close', text: 'Plain text - Обычный текст', date: '12.12.2012' },
  { id: 2, type: 'event_error_close', title: 'Close', text: 'Plain text - Обычный текст', date: '12.12.2012' },
  { id: 3, type: 'event_green_approve', title: 'Close', text: 'Plain text - Обычный текст', date: '12.12.2012' },
  { id: 4, type: 'green_ring', title: 'Close', text: 'Plain text - Обычный текст', date: '12.12.2012' },
  { id: 5, type: 'lk_edit', title: 'Close', text: 'Plain text - Обычный текст', date: '12.12.2012' },
  { id: 6, type: 'lk_enve_send', title: 'Close', text: 'Plain text - Обычный текст', date: '12.12.2012' },
  { id: 7, type: 'lk_rect_fill', title: 'Close', text: 'Plain text - Обычный текст', date: '12.12.2012' },
  { id: 8, type: 'red_ring', title: 'Close', text: 'Plain text - Обычный текст', date: '12.12.2012' },
  { id: 9, type: 'warn_yellow_inprogress', title: 'Close', text: 'Plain text - Обычный текст', date: '12.12.2012' },
  { id: 10, type: 'yellow_ring', title: 'Close', text: 'Plain text - Обычный текст', date: '12.12.2012' }
]

storiesOf('Основные составляющие страницы/Список уведомлений', module)
  .addDecorator(withKnobs)
  .add('NotificationList', withInfo('')(() => (
    <NotificationList notifications={DEFAULT_NOTIFICATIONS} />
  )))