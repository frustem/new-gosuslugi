import React from 'react'
// import PropTypes from 'prop-types'
import Notification from "../Notification"
import './NotificationList.sass'

const NotificationList = ({  notifications, className }) => (
  <div className={`notification-list ${className ? className : ''}`}>
    {notifications.map(notification => (
      <Notification key={notification.id} type={notification.type} title={notification.title} date={notification.date} >
        {notification.text}
      </Notification>
      )
    )}
  </div>
)
//
// Notification.propTypes = {
//   type: PropTypes.string.isRequired,
//   title: PropTypes.string.isRequired,
//   date: PropTypes.string.isRequired
// }

export default NotificationList