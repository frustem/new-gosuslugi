import React from 'react'
import {storiesOf} from '@storybook/react'
import {withInfo} from '@storybook/addon-info'
import {withKnobs, select, text} from '@storybook/addon-knobs/react'
import Icon, {ICON_TYPES} from './index'

const typeOptions = ICON_TYPES.reduce((acc, value) => {
  acc[value] = value
  return acc
}, {})

storiesOf('Графические элементы/Иконки', module)
  .addDecorator(withKnobs)
  .add('Icon', withInfo('')(() => (
    <div style={{width: '40px'}}>
      <Icon
        key="2"
        className="test"
        fill={text('color(fill) icon', '')}
        type={select('type', typeOptions, 'service_1')}
      />
    </div>
  )))
