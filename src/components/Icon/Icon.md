---
### Иконки услуг `<Icon />`

Используйте KNOBS (панель снизу) для изменения значений Property

<a href="http://guides.gosuslugi.ru/gayd-po-sozdaniyu-interfeysa-epgu/stileobrazuyuschie-elementy-i-katalog-uslug/iconki-uslug.html" target="_blank">Иконки услуг Онлайн Гайдбук</a>

| Property  | PropType         | Values          | DefaultProps |
| --------- | ---------------- | --------------- | ------------ |
| fill      | string           |                 | ''           |
| type      | number or string |                 | 1            |

---
