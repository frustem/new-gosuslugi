import PropTypes from 'prop-types'
import React from 'react'
import {ReactComponent as service_1} from './icons/service/1.svg'
import {ReactComponent as service_2} from './icons/service/2.svg'
import {ReactComponent as service_3} from './icons/service/3.svg'
import {ReactComponent as service_4} from './icons/service/4.svg'
import {ReactComponent as service_5} from './icons/service/5.svg'
import {ReactComponent as service_6} from './icons/service/6.svg'
import {ReactComponent as service_7} from './icons/service/7.svg'
import {ReactComponent as service_8} from './icons/service/8.svg'
import {ReactComponent as service_9} from './icons/service/9.svg'
import {ReactComponent as service_10} from './icons/service/10.svg'
import {ReactComponent as service_11} from './icons/service/11.svg'
import {ReactComponent as service_12} from './icons/service/12.svg'
import {ReactComponent as service_13} from './icons/service/13.svg'
import {ReactComponent as service_14} from './icons/service/14.svg'
import {ReactComponent as service_15} from './icons/service/15.svg'
import {ReactComponent as service_16} from './icons/service/16.svg'
import {ReactComponent as service_17} from './icons/service/17.svg'
import {ReactComponent as service_18} from './icons/service/18.svg'
import {ReactComponent as service_19} from './icons/service/19.svg'
import {ReactComponent as service_20} from './icons/service/20.svg'
import {ReactComponent as service_22} from './icons/service/22.svg'
import {ReactComponent as service_23} from './icons/service/23.svg'
import {ReactComponent as service_24} from './icons/service/24.svg'
import {ReactComponent as service_26} from './icons/service/26.svg'
import {ReactComponent as service_27} from './icons/service/27.svg'
import {ReactComponent as service_28} from './icons/service/28.svg'
import {ReactComponent as service_29} from './icons/service/29.svg'
import {ReactComponent as service_30} from './icons/service/30.svg'
import {ReactComponent as service_31} from './icons/service/31.svg'
import {ReactComponent as service_32} from './icons/service/32.svg'
import {ReactComponent as service_31_2} from './icons/service/31_2.svg'
import {ReactComponent as service_33} from './icons/service/33.svg'
import {ReactComponent as service_34} from './icons/service/34.svg'
import {ReactComponent as service_35} from './icons/service/35.svg'
import {ReactComponent as service_36} from './icons/service/36.svg'
import {ReactComponent as service_37} from './icons/service/37.svg'
import {ReactComponent as service_38} from './icons/service/38.svg'
import {ReactComponent as service_39} from './icons/service/39.svg'
import {ReactComponent as service_40} from './icons/service/40.svg'
import {ReactComponent as service_41} from './icons/service/41.svg'
import {ReactComponent as service_42} from './icons/service/42.svg'
import {ReactComponent as service_43} from './icons/service/43.svg'
import {ReactComponent as service_44} from './icons/service/44.svg'
import {ReactComponent as service_49} from './icons/service/49.svg'
import {ReactComponent as service_51} from './icons/service/51.svg'
import {ReactComponent as service_52} from './icons/service/52.svg'
import {ReactComponent as service_53} from './icons/service/53.svg'
import {ReactComponent as service_54} from './icons/service/54.svg'
import {ReactComponent as service_55} from './icons/service/55.svg'
import {ReactComponent as service_56} from './icons/service/56.svg'
import {ReactComponent as service_62} from './icons/service/62.svg'
import {ReactComponent as service_64} from './icons/service/64.svg'
import {ReactComponent as service_66} from './icons/service/66.svg'
import {ReactComponent as service_67} from './icons/service/67.svg'
import {ReactComponent as files_all} from './icons/files/all.svg'
import {ReactComponent as files_doc} from './icons/files/doc.svg'
import {ReactComponent as files_jpeg} from './icons/files/jpeg.svg'
import {ReactComponent as files_jpg} from './icons/files/jpg.svg'
import {ReactComponent as files_mp3} from './icons/files/mp3.svg'
import {ReactComponent as files_mp4} from './icons/files/mp4.svg'
import {ReactComponent as files_pdf} from './icons/files/pdf.svg'
import {ReactComponent as files_ppt} from './icons/files/ppt.svg'
import {ReactComponent as files_pptx} from './icons/files/pptx.svg'
import {ReactComponent as files_rtf} from './icons/files/rtf.svg'
import {ReactComponent as files_tif} from './icons/files/tif.svg'
import {ReactComponent as files_tiff} from './icons/files/tiff.svg'
import {ReactComponent as files_txt} from './icons/files/txt.svg'
import {ReactComponent as files_wav} from './icons/files/wav.svg'
import {ReactComponent as files_xls} from './icons/files/xls.svg'
import {ReactComponent as files_xlsx} from './icons/files/xlsx.svg'
import {ReactComponent as files_zip} from './icons/files/zip.svg'
import {ReactComponent as close} from './icons/notification/close.svg'
import {ReactComponent as event_error_close} from './icons/notification/event_error_close.svg'
import {ReactComponent as event_green_approve} from './icons/notification/event_green_approve.svg'
import {ReactComponent as green_ring} from './icons/notification/green_ring.svg'
import {ReactComponent as lk_edit} from './icons/notification/lk_edit.svg'
import {ReactComponent as lk_enve_send} from './icons/notification/lk_enve_send.svg'
import {ReactComponent as lk_rect_fill} from './icons/notification/lk_rect_fill.svg'
import {ReactComponent as red_ring} from './icons/notification/red_ring.svg'
import {ReactComponent as warn_yellow_inprogress} from './icons/notification/warn_yellow_inprogress.svg'
import {ReactComponent as yellow_ring} from './icons/notification/yellow_ring.svg'
import {ReactComponent as like} from './icons/result/like.svg'
import {ReactComponent as ok} from './icons/result/ok.svg'
import {ReactComponent as warning} from './icons/result/warning.svg'
import {ReactComponent as bad} from './icons/result/bad.svg'
import {ReactComponent as hex_approve_green} from './icons/informer/hex_approve_green.svg'
import {ReactComponent as hex_switch} from './icons/informer/hex_switch.svg'
import {ReactComponent as hex_warn_red} from './icons/informer/hex_warn_red.svg'
import {ReactComponent as mail_icon} from './icons/informer/mail_icon.svg'
import {ReactComponent as settings_gray} from './icons/informer/settings_gray.svg'
import {ReactComponent as settings_white} from './icons/informer/settings_white.svg'
import help from './icons/button/help'
import help_nut from './icons/button/help_nut'
import search from './icons/button/search'
import complaint from './icons/button/complaint'
import trash from './icons/button/trash'
import logo_gosuslugi from './icons/other/logo_gosuslugi'
import clock from './icons/other/clock'
import './icon.sass'


const icons = {
  service_1, service_2, service_3,service_4,service_5, service_6,service_7,service_8,service_9,service_10,service_11,service_12,service_13,service_14,service_15,service_16,service_17,service_18,service_19,service_20,service_22,service_23,service_24,service_26,
  service_27,service_28,service_29,service_30,service_31,service_31_2,service_32,service_33,service_34,service_35,service_36,service_37,service_38,service_39,service_40,service_41,service_42,service_43,service_44,service_49,service_51,
  service_52,service_53,service_54,service_55,service_56,service_62,service_64,service_66,service_67,//services
  files_all, files_doc, files_jpeg, files_jpg, files_mp3, files_mp4, files_pdf, files_ppt, files_pptx, files_rtf, files_tif,
  files_tiff, files_txt, files_wav, files_xls, files_xlsx, files_zip,  // files
  help, search, complaint, help_nut, trash, //button
  logo_gosuslugi, clock,  //other
  close, event_error_close, event_green_approve, green_ring, lk_edit, lk_enve_send, lk_rect_fill,
  red_ring,warn_yellow_inprogress,yellow_ring, //notifications
  bad,like, ok, warning, //results
  hex_approve_green, hex_switch, hex_warn_red, mail_icon, settings_gray, settings_white, //informers

  
}

export const ICON_TYPES = Object.keys(icons)

const Icon = ({ className, fill="", type='', ...props }) => {
  let defaultType = 'service_1'
  let trueType = type ? type : defaultType
  let icon = icons[trueType]
  let TrueIcon = icon ? icon : icons[defaultType]
  return (
    <TrueIcon fill={fill} width="100%" {...props} />
)
}

Icon.propTypes = {
  fill: PropTypes.string,
  type: PropTypes.oneOf(Object.keys(icons))
}

Icon.defaultProps = {
  fill: ''
}

export default Icon
