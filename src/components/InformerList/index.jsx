import React from 'react'
import PropTypes from 'prop-types'
import Informer from '../Informer'
import './InformerList.sass'

const InformerList = ({  informers, className }) => (
  <div className={`informer-list ${className ? className : ''}`}>
    {informers.map(informer => (
      <Informer key={informer.type} type={informer.type} title={informer.title}>
        {informer.text}
      </Informer>
      )
    )}
  </div>
)


InformerList.propTypes = {
  informers: PropTypes.arrayOf(PropTypes.object).isRequired
}

export default InformerList