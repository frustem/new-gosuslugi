import React from 'react'
import {storiesOf} from '@storybook/react'
import {withInfo} from '@storybook/addon-info'
import {withKnobs} from '@storybook/addon-knobs/react'
import InformerList from './index'

const INFORMER_ICON_TYPES = [
  'hex_approve_green',
  'hex_switch',
  'hex_warn_red',
  'mail_icon',
  'settings_gray',
  'settings_white'
]

const typeOptions = INFORMER_ICON_TYPES.map((value, ind) => ({
  id: ind, type: value, title: 'Title' + ind, text: 'Текст события - Plain text.' + ind
  })
)

storiesOf('Основные составляющие страницы/Список Информеров', module)
  .addDecorator(withKnobs)
  .add('InformerList', withInfo('')(() => (
    <InformerList informers={typeOptions} />
  )))