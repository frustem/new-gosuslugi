import React from 'react'
import {storiesOf} from '@storybook/react'
import {withKnobs} from '@storybook/addon-knobs/react'
import Tabs from './Tabs'
import Tab from 'react-bootstrap/Tab'

storiesOf('Интерактивные элементы управления и навигации/Вкладки', module)
  .addDecorator(withKnobs)
  .add('Tabs', () => (
      <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example">
        <Tab eventKey="home" title="Home">
          <span>
            Unthrifty loveliness, why dost thou spend Upon thy self thy beauty's legacy? Nature's bequest gives nothing,
            but doth lend, And being frank she lends to those are free: Then, beauteous niggard, why dost thou abuse The
            bounteous largess given thee to give? Profitless usurer, why dost thou use So great a sum of sums, yet canst
            not live? For having traffic with thy self alone, Thou of thy self thy sweet self dost deceive:
          </span>
        </Tab>
        <Tab eventKey="profile" title="Profile">
          <span>
           Lord of my love, to whom in vassalage Thy merit hath my duty strongly knit, To thee I send this written
            embassage, To witness duty, not to show my wit: Duty so great, which wit so poor as mine May make seem bare,
            in wanting words to show it, But that I hope some good conceit of thine In thy soul's thought, all naked,
            will bestow it: Till whatsoever star that guides my moving, Points on me graciously with fair aspect,
          </span>
        </Tab>
        <Tab eventKey="contact" title="Contact" disabled>
          <span>
            Unthrifty loveliness, why dost thou spend Upon thy self thy beauty's legacy? Nature's bequest gives nothing,
            but doth lend, And being frank she lends to those are free: Then, beauteous niggard, why dost thou abuse The
            bounteous largess given thee to give? Profitless usurer, why dost thou use So great a sum of sums, yet canst
            not live? For having traffic with thy self alone, Thou of thy self thy sweet self dost deceive:
          </span>
        </Tab>
      </Tabs>
    )
  )

