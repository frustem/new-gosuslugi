import React from 'react'
import PropTypes from 'prop-types'
import './Notification.sass'
import Icon from "../Icon"
import Arrow from "../internal/Arrow"

const Notification = ({  type, title, date, children }) => (
  <div className="notification">
    <div className="icon-notification">
      <Icon type={type} />
    </div>
    <span className="notification__caption">{title}</span>
    <span className="notification__text">
      {children}
    </span>
    <span className="notification__date">
      <span className="date__clock-wrapper">
        <Icon type='clock' />
      </span>
      {date}
      <Arrow direction="right" size="sm" style={{margin: '9px'}} />
    </span>
  </div>
)

Notification.propTypes = {
  type: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired
}

export default Notification