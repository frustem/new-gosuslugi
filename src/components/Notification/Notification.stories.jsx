import React from 'react'
import {storiesOf} from '@storybook/react'
import {withInfo} from '@storybook/addon-info'
import {withKnobs, text, select} from '@storybook/addon-knobs/react'
import Notification from './index'

const typeOptions = { 
	close: 'close',
  event_error_close: 'event_error_close',
  event_green_approve: 'event_green_approve',
  green_ring: 'green_ring',
  lk_edit: 'lk_edit',
  lk_enve_send: 'lk_enve_send',
  lk_rect_fill: 'lk_rect_fill',
  red_ring: 'red_ring',
  warn_yellow_inprogress: 'warn_yellow_inprogress',
  yellow_ring: 'yellow_ring'
}
const DEFAULT_TEXT = 'Текст события - Plain text.'
const DEFAULT_DATE = '12.12.2018'

storiesOf('Основные составляющие страницы/Уведомление', module)
  .addDecorator(withKnobs)
  .add('Notification', withInfo('')(() => (
    <Notification 
      type={select('type', typeOptions, 'close')}
      title={text('title', 'Title')} 
      date={text('date', DEFAULT_DATE)}
    >
      {text('text', DEFAULT_TEXT)}
    </Notification>
  )))