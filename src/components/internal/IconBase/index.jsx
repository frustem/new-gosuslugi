// import React from 'react'
// import PropTypes from 'prop-types'
// import { ReactComponent as IC } from './icons/notification/lk_edit.svg'

// const ICONS = {
//   notification: {
//     close: 'notification/close',
//     event_error_close: 'notification/event_error_close',
//     event_green_approve: 'notification/event_green_approve',
//     green_ring: 'notification/green_ring',
//     lk_edit: 'notification/lk_edit',
//     lk_enve_send: 'notification/lk_enve_send',
//     lk_rect_fill: 'notification/lk_rect_fill',
//     red_ring: 'notification/red_ring',
//     warn_yellow_inprogress: 'notification/warn_yellow_inprogress',
//     yellow_ring: 'notification/yellow_ring'
//   },
//   result: {
//     bad: 'result/bad',
//     like: 'result/like',
//     ok: 'result/ok',
//     warning: 'result/warning'
//   },
//   informer: {
//     hex_approve_green: 'informer/hex_approve_green',
//     hex_switch: 'informer/hex_switch',
//     hex_warn_red: 'informer/hex_warn_red',
//     mail_icon: 'informer/mail_icon',
//     settings_gray: 'informer/settings_gray',
//     settings_white: 'informer/settings_white'
//   },
//   service: {
//     service1: 'service/1',
//     service2: 'service/2',
//     service3: 'service/3',
//     service4: 'service/4',
//     service5: 'service/5',
//     service6: 'service/6',
//     service7: 'service/7',
//     service8: 'service/8',
//     service9: 'service/9',
//     service10: 'service/10',
//     service12: 'service/12',
//     service13: 'service/13',
//     service14: 'service/14',
//     service15: 'service/15',
//     service16: 'service/16',
//     service17: 'service/17',
//     service18: 'service/18',
//     service19: 'service/19',
//     service20: 'service/20',
//     service21: 'service/21',
//     service22: 'service/22',
//     service23: 'service/23',
//     service24: 'service/24',
//     service25: 'service/25',
//     service26: 'service/26',
//     service27: 'service/27',
//     service28: 'service/28',
//     service29: 'service/29',
//     service30: 'service/30',
//     service31: 'service/31',
//     service31_2: 'service/31_2',
//     service32: 'service/32',
//     service33: 'service/33',
//     service34: 'service/34',
//     service35: 'service/35',
//     service36: 'service/36',
//     service37: 'service/37',
//     service38: 'service/38',
//     service39: 'service/39',
//     service40: 'service/40',
//     service41: 'service/41',
//     service42: 'service/42',
//     service43: 'service/43',
//     service44: 'service/44',
//     service49: 'service/49',
//     service51: 'service/51',
//     service52: 'service/52',
//     service53: 'service/53',
//     service54: 'service/54',
//     service55: 'service/55',
//     service56: 'service/56',
//     service57: 'service/57',
//     service58: 'service/58',
//     service59: 'service/59',
//     service60: 'service/60',
//     service61: 'service/61',
//     service62: 'service/62',
//     service63: 'service/63',
//     service64: 'service/64',
//     service65: 'service/65',
//     service66: 'service/66',
//     service67: 'service/67',
//   },
//   files: {
//     all: 'files/all@1x',
//     doc: 'files/doc@1x',
//     jpeg: 'files/jpeg@1x',
//     jpg: 'files/jpg@1x',
//     mp3: 'files/mp3@1x',
//     mp4: 'files/mp4@1x',
//     pdf: 'files/pdf@1x',
//     ppt: 'files/ppt@1x',
//     pptx: 'files/pptx@1x',
//     rtf: 'files/rtf@1x',
//     tif: 'files/tif@1x',
//     tiff: 'files/tiff@1x',
//     txt: 'files/txt@1x',
//     wav: 'files/wav@1x',
//     xls: 'files/xls@1x',
//     xlsx: 'files/xlsx@1x',
//     zip: 'files/zip@1x'
//   }
// }

// export const NOTIFICATION_ICON_TYPES = Object.keys(ICONS.notification)
// export const RESULT_ICON_TYPES = Object.keys(ICONS.result)
// export const INFORMER_ICON_TYPES = Object.keys(ICONS.informer)
// export const SERVICE_ICON_TYPES = Object.keys(ICONS.service)
// export const FILES_ICON_TYPES = Object.keys(ICONS.files)
// export const ICON_TYPES = [].concat(NOTIFICATION_ICON_TYPES, RESULT_ICON_TYPES, INFORMER_ICON_TYPES, SERVICE_ICON_TYPES, FILES_ICON_TYPES)

// const IconBase = ({ type }) => {
//   let key = Object.keys(ICONS).find(key => ICONS[key][type])
//   let iconSrc = require('./icons/' + ICONS[key][type] + '.svg') //eslint-disable-line
//   let {ReactComponent: Icon} = require('./icons/' + ICONS[key][type] + '.svg')
//   return (
//     <div>
//       <img className="IconBase" src={iconSrc} alt="" width="100%" height="auto" />
//       <Icon/>
//     </div>
//   )
// }
// IconBase.propTypes = {
//   type: PropTypes.oneOf([...ICON_TYPES]).isRequired
// }

// export default IconBase