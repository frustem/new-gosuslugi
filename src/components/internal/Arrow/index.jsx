import React from 'react'
import PropTypes from 'prop-types'
import './Arrow.sass'

const Arrow = ({direction, size, color, style}) => <span className={`arrow arrow__${direction} arrow__${size} arrow__${color}`} style={style} />

Arrow.propTypes = {
  direction: PropTypes.string,
  size: PropTypes.string,
  color: PropTypes.string
}

Arrow.defaultProps = {
  direction: 'right',
  color: 'default',
  size: 'md'
}

export default Arrow
