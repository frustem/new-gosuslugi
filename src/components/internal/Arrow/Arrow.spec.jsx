import React from 'react'
import renderer from 'react-test-renderer'
import Arrow from './index'

const direction = 'right'
const size = 'md'

test('Arrow with blank params', () => {
  const component = renderer.create(
    <Arrow />
  )
  let tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})

test('Arrow with all params', () => {
  const component = renderer.create(
    <Arrow direction={direction} size={size} />
  )
  let tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})
