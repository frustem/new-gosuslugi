import React from 'react'
import {storiesOf} from '@storybook/react'
import {withInfo} from '@storybook/addon-info'
import {withKnobs, select} from '@storybook/addon-knobs/react'
import Arrow from './index'

const Size = 'size'
const optionsSize = {
  lg: 'lg',
  md: 'md',
  sm: 'sm',
  xs: 'xs'
}
const defaultValueSize = 'md'

const direction = 'Direction'
const optionsDirection = {
  right: 'right',
  top: 'top',
  bottom: 'bottom',
  left: 'left'
}
const defaultDirection = 'right'

const Color = 'Color'
const optionsColor = {
  default: 'default',
  success: 'success',
  danger: 'danger',
  warning: 'warning',
  primary: 'primary',
  info: 'info'
}
const defaultColor = 'default'

storiesOf('Типографика/Стрелка', module)
  .addDecorator(withKnobs)
  .add('Arrow', withInfo()(() => (
    <Arrow
      key="2"
      size={select(Size, optionsSize, defaultValueSize)}
      direction={select(direction, optionsDirection, defaultDirection)}
      color={select(Color, optionsColor, defaultColor)}
    />
    // {/*<Markdown key="1" info={MD} />*/}
    ))
  )
  