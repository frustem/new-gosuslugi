---
### Компонент `<Arrow />`
Используйте KNOBS (панель снизу) для изменения значений Property

| Property  | PropType | Values                                           | DefaultProps |
| --------- | -------- | ------------------------------------------------ | ------------ |
| direction | string   | left, right, bottom, left                        | right        |
| size      | string   | xl, md, sm, xs                                   | md           |
| color     | string   | success, danger, info, warning, primary ,default | default      |

---
