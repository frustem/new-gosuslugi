import React from 'react'
import {storiesOf} from '@storybook/react'
// import {withInfo} from '@storybook/addon-info'
import {withKnobs, select, text, boolean} from '@storybook/addon-knobs/react'
import Button from './Button'

const variants = {
  default: 'default',
  primary: 'primary',
  secondary: 'secondary',
  success: 'success',
  info: 'info',
  warning: 'warning',
  danger: 'danger',
  light: 'light',
  dark: 'dark'
}

const sizes = {
  sm: 'sm',
  md: 'md',
  lg: 'lg'
}


storiesOf('Интерактивные элементы управления и навигации/Кнопки', module)
  .addDecorator(withKnobs)
  // .addDecorator(withInfo)
  .add('Button', () => (
    <Button
      variant={select('variant', variants, 'default')}
      size={select('size', sizes, 'md')}
      disabled={boolean('Disabled', false)}
    >
      {text('Button text', 'Button text')}
    </Button>
  ))