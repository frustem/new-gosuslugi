import React from 'react'
import Icon from '../Icon'
import './Result.sass'

const Result = ({  type, title, children }) => (
  <div className="result">
    <hr />
    <div className="result__caption-wrapper">
      <div className="iconResult">
        <Icon type={type} />
      </div>
      <h3 className="result__caption">{title}</h3>
    </div>
    <div className="result__text">
      {children}
    </div>
  </div>
)

export default Result