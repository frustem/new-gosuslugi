import React from 'react'
import {storiesOf} from '@storybook/react'
import {withInfo} from '@storybook/addon-info'
import {withKnobs, text, select} from '@storybook/addon-knobs/react'
import Result from './index'

const typeOptions = { 
	ok: 'ok', 
	bad: 'bad', 
	like: 'like', 
	warning: 'warning' 
}

const DEFAULT_TEXT = 'Текст события - Plain text. Следите за его статусом в личном кабинете, по электронной почте или SMS-сообщениях.'

storiesOf('Основные составляющие страницы/Результат', module)
  .addDecorator(withKnobs)
  .add('Result', withInfo('')(() => (
		<div className="container">
      <Result type={select('type', typeOptions, 'ok')} title={text('title', 'Title')}>
        {text('text', DEFAULT_TEXT)}
      </Result>
		</div>
  
  )))