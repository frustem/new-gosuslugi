module.exports = {
  parser: "babel-eslint",
  'parserOptions': {
    'ecmaFeatures': {
      'jsx': true
    }
  },
  'rules': {
    'comma-dangle': ['error', 'never'],
    'no-console': 'warn',
    'react/forbid-prop-types': 'warn',
    'import/no-unresolved': 'off',
    'import/no-extraneous-dependencies': 'off',
    'import/extensions': 'off',
    'jsx-a11y/href-no-hash': 'off',
    "jsx-a11y/anchor-is-valid": ["warn", { "aspects": ["invalidHref"] }],
    semi: ['error', 'never'],
    'react/prop-types': 'off',
  },
  "plugins": [
    "react",
    "jsx",
  ],
  'env': {
    'es6':true,
    'browser': true,
    'node': true,
    'jest': true,
  },
  "extends": ['airbnb', "eslint:recommended", "plugin:react/recommended"],
};
